﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutomobilePartsStorage
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }


        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        Point lastPoint;
        private void panel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void Krd_Click(object sender, EventArgs e)
        {
            Краснодар newForm = new Краснодар();
            newForm.Show();
            this.Hide();
        }

        private void RnD_Click(object sender, EventArgs e)
        {
            Ростов_на_Дону newForm = new Ростов_на_Дону();
            newForm.Show();
            this.Hide();
        }

        private void Vld_Click(object sender, EventArgs e)
        {
            Волгоград newForm = new Волгоград();
            newForm.Show();
            this.Hide();
        }
    }
}
