﻿
namespace AutomobilePartsStorage
{
    partial class Base
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Base));
            this.panel = new System.Windows.Forms.Panel();
            this.StorageLabel = new System.Windows.Forms.Label();
            this.Add = new System.Windows.Forms.Button();
            this.IndexLabel = new System.Windows.Forms.Label();
            this.PriceLabel = new System.Windows.Forms.Label();
            this.CategoryLabel = new System.Windows.Forms.Label();
            this.MarkLabel = new System.Windows.Forms.Label();
            this.CountryLabel = new System.Windows.Forms.Label();
            this.IndexBox = new System.Windows.Forms.TextBox();
            this.PriceBox = new System.Windows.Forms.TextBox();
            this.CategoryBox = new System.Windows.Forms.TextBox();
            this.MarkBox = new System.Windows.Forms.TextBox();
            this.CountryBox = new System.Windows.Forms.TextBox();
            this.Company = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Info = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Label();
            this.pictureStripe2 = new System.Windows.Forms.PictureBox();
            this.pictureStripe1 = new System.Windows.Forms.PictureBox();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.White;
            this.panel.Controls.Add(this.StorageLabel);
            this.panel.Controls.Add(this.Add);
            this.panel.Controls.Add(this.IndexLabel);
            this.panel.Controls.Add(this.PriceLabel);
            this.panel.Controls.Add(this.CategoryLabel);
            this.panel.Controls.Add(this.MarkLabel);
            this.panel.Controls.Add(this.CountryLabel);
            this.panel.Controls.Add(this.IndexBox);
            this.panel.Controls.Add(this.PriceBox);
            this.panel.Controls.Add(this.CategoryBox);
            this.panel.Controls.Add(this.MarkBox);
            this.panel.Controls.Add(this.CountryBox);
            this.panel.Controls.Add(this.Company);
            this.panel.Controls.Add(this.Back);
            this.panel.Controls.Add(this.textBox1);
            this.panel.Controls.Add(this.Info);
            this.panel.Controls.Add(this.closeButton);
            this.panel.Controls.Add(this.pictureStripe2);
            this.panel.Controls.Add(this.pictureStripe1);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1068, 591);
            this.panel.TabIndex = 0;
            this.panel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDown);
            this.panel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_MouseMove_1);
            // 
            // StorageLabel
            // 
            this.StorageLabel.AutoSize = true;
            this.StorageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StorageLabel.Location = new System.Drawing.Point(741, 74);
            this.StorageLabel.Name = "StorageLabel";
            this.StorageLabel.Size = new System.Drawing.Size(87, 29);
            this.StorageLabel.TabIndex = 33;
            this.StorageLabel.Text = "Склад";
            // 
            // Add
            // 
            this.Add.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Add.FlatAppearance.BorderSize = 0;
            this.Add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.Add.Location = new System.Drawing.Point(378, 483);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(177, 70);
            this.Add.TabIndex = 32;
            this.Add.Text = "Добавить";
            this.Add.UseVisualStyleBackColor = false;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // IndexLabel
            // 
            this.IndexLabel.Location = new System.Drawing.Point(211, 302);
            this.IndexLabel.Name = "IndexLabel";
            this.IndexLabel.Size = new System.Drawing.Size(161, 26);
            this.IndexLabel.TabIndex = 27;
            this.IndexLabel.Text = "Индекс";
            // 
            // PriceLabel
            // 
            this.PriceLabel.Location = new System.Drawing.Point(211, 254);
            this.PriceLabel.Name = "PriceLabel";
            this.PriceLabel.Size = new System.Drawing.Size(161, 26);
            this.PriceLabel.TabIndex = 26;
            this.PriceLabel.Text = "Цена";
            // 
            // CategoryLabel
            // 
            this.CategoryLabel.Location = new System.Drawing.Point(211, 203);
            this.CategoryLabel.Name = "CategoryLabel";
            this.CategoryLabel.Size = new System.Drawing.Size(161, 26);
            this.CategoryLabel.TabIndex = 25;
            this.CategoryLabel.Text = "Категория";
            // 
            // MarkLabel
            // 
            this.MarkLabel.Location = new System.Drawing.Point(211, 158);
            this.MarkLabel.Name = "MarkLabel";
            this.MarkLabel.Size = new System.Drawing.Size(161, 26);
            this.MarkLabel.TabIndex = 24;
            this.MarkLabel.Text = "Марка автомобиля";
            // 
            // CountryLabel
            // 
            this.CountryLabel.Location = new System.Drawing.Point(211, 112);
            this.CountryLabel.Name = "CountryLabel";
            this.CountryLabel.Size = new System.Drawing.Size(161, 26);
            this.CountryLabel.TabIndex = 23;
            this.CountryLabel.Text = "Страна изготовитель";
            // 
            // IndexBox
            // 
            this.IndexBox.Location = new System.Drawing.Point(414, 299);
            this.IndexBox.Multiline = true;
            this.IndexBox.Name = "IndexBox";
            this.IndexBox.Size = new System.Drawing.Size(141, 26);
            this.IndexBox.TabIndex = 22;
            // 
            // PriceBox
            // 
            this.PriceBox.Location = new System.Drawing.Point(414, 251);
            this.PriceBox.Multiline = true;
            this.PriceBox.Name = "PriceBox";
            this.PriceBox.Size = new System.Drawing.Size(141, 26);
            this.PriceBox.TabIndex = 21;
            // 
            // CategoryBox
            // 
            this.CategoryBox.Location = new System.Drawing.Point(414, 203);
            this.CategoryBox.Multiline = true;
            this.CategoryBox.Name = "CategoryBox";
            this.CategoryBox.Size = new System.Drawing.Size(141, 26);
            this.CategoryBox.TabIndex = 20;
            // 
            // MarkBox
            // 
            this.MarkBox.Location = new System.Drawing.Point(414, 155);
            this.MarkBox.Multiline = true;
            this.MarkBox.Name = "MarkBox";
            this.MarkBox.Size = new System.Drawing.Size(141, 26);
            this.MarkBox.TabIndex = 19;
            // 
            // CountryBox
            // 
            this.CountryBox.Location = new System.Drawing.Point(414, 109);
            this.CountryBox.Multiline = true;
            this.CountryBox.Name = "CountryBox";
            this.CountryBox.Size = new System.Drawing.Size(141, 26);
            this.CountryBox.TabIndex = 18;
            // 
            // Company
            // 
            this.Company.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Company.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Company.FlatAppearance.BorderSize = 0;
            this.Company.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.Company.Location = new System.Drawing.Point(195, 483);
            this.Company.Name = "Company";
            this.Company.Size = new System.Drawing.Size(177, 70);
            this.Company.TabIndex = 17;
            this.Company.Text = "О A1 Motors";
            this.Company.UseVisualStyleBackColor = false;
            this.Company.Click += new System.EventHandler(this.Company_Click);
            // 
            // Back
            // 
            this.Back.AutoSize = true;
            this.Back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Back.Location = new System.Drawing.Point(12, 55);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(38, 29);
            this.Back.TabIndex = 16;
            this.Back.Text = "←";
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(587, 106);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(399, 444);
            this.textBox1.TabIndex = 15;
            // 
            // Info
            // 
            this.Info.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Info.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Info.FlatAppearance.BorderSize = 0;
            this.Info.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.Info.Location = new System.Drawing.Point(12, 483);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(177, 70);
            this.Info.TabIndex = 13;
            this.Info.Text = "Информация и силовых агрегатах";
            this.Info.UseVisualStyleBackColor = false;
            this.Info.Click += new System.EventHandler(this.Info_Click_1);
            // 
            // closeButton
            // 
            this.closeButton.AutoSize = true;
            this.closeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.closeButton.Location = new System.Drawing.Point(1031, 9);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(25, 29);
            this.closeButton.TabIndex = 12;
            this.closeButton.Text = "x";
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            this.closeButton.MouseEnter += new System.EventHandler(this.closeButton_MouseEnter_1);
            this.closeButton.MouseLeave += new System.EventHandler(this.closeButton_MouseLeave_1);
            this.closeButton.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_MouseMove_1);
            // 
            // pictureStripe2
            // 
            this.pictureStripe2.Image = global::AutomobilePartsStorage.Properties.Resources.Blue_stripe;
            this.pictureStripe2.Location = new System.Drawing.Point(1019, 41);
            this.pictureStripe2.Name = "pictureStripe2";
            this.pictureStripe2.Size = new System.Drawing.Size(49, 550);
            this.pictureStripe2.TabIndex = 11;
            this.pictureStripe2.TabStop = false;
            // 
            // pictureStripe1
            // 
            this.pictureStripe1.Image = global::AutomobilePartsStorage.Properties.Resources.Blue_stripe;
            this.pictureStripe1.Location = new System.Drawing.Point(0, 0);
            this.pictureStripe1.Name = "pictureStripe1";
            this.pictureStripe1.Size = new System.Drawing.Size(1020, 42);
            this.pictureStripe1.TabIndex = 10;
            this.pictureStripe1.TabStop = false;
            // 
            // Base
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1068, 591);
            this.Controls.Add(this.panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Base";
            this.Text = "Base";
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.PictureBox pictureStripe1;
        private System.Windows.Forms.PictureBox pictureStripe2;
        private System.Windows.Forms.Label closeButton;
        private System.Windows.Forms.Button Info;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label Back;
        private System.Windows.Forms.Button Company;
        private System.Windows.Forms.TextBox IndexBox;
        private System.Windows.Forms.TextBox PriceBox;
        private System.Windows.Forms.TextBox CategoryBox;
        private System.Windows.Forms.TextBox MarkBox;
        private System.Windows.Forms.TextBox CountryBox;
        private System.Windows.Forms.Label IndexLabel;
        private System.Windows.Forms.Label PriceLabel;
        private System.Windows.Forms.Label CategoryLabel;
        private System.Windows.Forms.Label MarkLabel;
        private System.Windows.Forms.Label CountryLabel;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Label StorageLabel;
    }
}