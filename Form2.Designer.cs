﻿
namespace AutomobilePartsStorage
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel = new System.Windows.Forms.Panel();
            this.pictureStripe1 = new System.Windows.Forms.PictureBox();
            this.pictureStripe2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.closeButton = new System.Windows.Forms.Label();
            this.RnD = new System.Windows.Forms.Button();
            this.Krd = new System.Windows.Forms.Button();
            this.Vld = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.White;
            this.panel.Controls.Add(this.label1);
            this.panel.Controls.Add(this.Vld);
            this.panel.Controls.Add(this.Krd);
            this.panel.Controls.Add(this.RnD);
            this.panel.Controls.Add(this.pictureStripe1);
            this.panel.Controls.Add(this.pictureStripe2);
            this.panel.Controls.Add(this.pictureBox3);
            this.panel.Controls.Add(this.closeButton);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Margin = new System.Windows.Forms.Padding(2);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(673, 450);
            this.panel.TabIndex = 3;
            this.panel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDown);
            this.panel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_MouseMove);
            // 
            // pictureStripe1
            // 
            this.pictureStripe1.Image = global::AutomobilePartsStorage.Properties.Resources.Blue_stripe;
            this.pictureStripe1.Location = new System.Drawing.Point(0, 0);
            this.pictureStripe1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureStripe1.Name = "pictureStripe1";
            this.pictureStripe1.Size = new System.Drawing.Size(639, 34);
            this.pictureStripe1.TabIndex = 9;
            this.pictureStripe1.TabStop = false;
            // 
            // pictureStripe2
            // 
            this.pictureStripe2.Image = global::AutomobilePartsStorage.Properties.Resources.Blue_stripe;
            this.pictureStripe2.Location = new System.Drawing.Point(636, 33);
            this.pictureStripe2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureStripe2.Name = "pictureStripe2";
            this.pictureStripe2.Size = new System.Drawing.Size(37, 414);
            this.pictureStripe2.TabIndex = 2;
            this.pictureStripe2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::AutomobilePartsStorage.Properties.Resources.A1_Motors_logo1;
            this.pictureBox3.Location = new System.Drawing.Point(29, 38);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(89, 94);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // closeButton
            // 
            this.closeButton.AutoSize = true;
            this.closeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.closeButton.Location = new System.Drawing.Point(647, 7);
            this.closeButton.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(22, 25);
            this.closeButton.TabIndex = 2;
            this.closeButton.Text = "x";
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // RnD
            // 
            this.RnD.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.RnD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RnD.FlatAppearance.BorderSize = 0;
            this.RnD.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.RnD.Location = new System.Drawing.Point(265, 277);
            this.RnD.Margin = new System.Windows.Forms.Padding(2);
            this.RnD.Name = "RnD";
            this.RnD.Size = new System.Drawing.Size(133, 57);
            this.RnD.TabIndex = 102;
            this.RnD.Text = "Ростов-на-Дону";
            this.RnD.UseVisualStyleBackColor = false;
            this.RnD.Click += new System.EventHandler(this.RnD_Click);
            // 
            // Krd
            // 
            this.Krd.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Krd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Krd.FlatAppearance.BorderSize = 0;
            this.Krd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.Krd.Location = new System.Drawing.Point(80, 277);
            this.Krd.Margin = new System.Windows.Forms.Padding(2);
            this.Krd.Name = "Krd";
            this.Krd.Size = new System.Drawing.Size(133, 57);
            this.Krd.TabIndex = 103;
            this.Krd.Text = "Краснодар";
            this.Krd.UseVisualStyleBackColor = false;
            this.Krd.Click += new System.EventHandler(this.Krd_Click);
            // 
            // Vld
            // 
            this.Vld.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Vld.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Vld.FlatAppearance.BorderSize = 0;
            this.Vld.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.Vld.Location = new System.Drawing.Point(449, 277);
            this.Vld.Margin = new System.Windows.Forms.Padding(2);
            this.Vld.Name = "Vld";
            this.Vld.Size = new System.Drawing.Size(133, 57);
            this.Vld.TabIndex = 104;
            this.Vld.Text = "Волгоград";
            this.Vld.UseVisualStyleBackColor = false;
            this.Vld.Click += new System.EventHandler(this.Vld_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(32, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(588, 31);
            this.label1.TabIndex = 105;
            this.label1.Text = "Выберите город для просмотра информации:\r\n";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(673, 450);
            this.Controls.Add(this.panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form2";
            this.Text = "Form2";
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.PictureBox pictureStripe1;
        private System.Windows.Forms.PictureBox pictureStripe2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label closeButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Vld;
        private System.Windows.Forms.Button Krd;
        private System.Windows.Forms.Button RnD;
    }
}