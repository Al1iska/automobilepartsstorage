﻿
namespace AutomobilePartsStorage
{
    partial class Ростов_на_Дону
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ростов_на_Дону));
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureStripe2 = new System.Windows.Forms.PictureBox();
            this.Back = new System.Windows.Forms.Label();
            this.pictureStripe1 = new System.Windows.Forms.PictureBox();
            this.Info = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.Info);
            this.panel2.Controls.Add(this.pictureStripe2);
            this.panel2.Controls.Add(this.Back);
            this.panel2.Controls.Add(this.pictureStripe1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(827, 544);
            this.panel2.TabIndex = 2;
            // 
            // pictureStripe2
            // 
            this.pictureStripe2.Image = global::AutomobilePartsStorage.Properties.Resources.Blue_stripe;
            this.pictureStripe2.Location = new System.Drawing.Point(0, 509);
            this.pictureStripe2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureStripe2.Name = "pictureStripe2";
            this.pictureStripe2.Size = new System.Drawing.Size(826, 34);
            this.pictureStripe2.TabIndex = 19;
            this.pictureStripe2.TabStop = false;
            // 
            // Back
            // 
            this.Back.AutoSize = true;
            this.Back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Back.Location = new System.Drawing.Point(9, 37);
            this.Back.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(32, 25);
            this.Back.TabIndex = 17;
            this.Back.Text = "←";
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // pictureStripe1
            // 
            this.pictureStripe1.Image = global::AutomobilePartsStorage.Properties.Resources.Blue_stripe;
            this.pictureStripe1.Location = new System.Drawing.Point(0, 0);
            this.pictureStripe1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureStripe1.Name = "pictureStripe1";
            this.pictureStripe1.Size = new System.Drawing.Size(826, 34);
            this.pictureStripe1.TabIndex = 11;
            this.pictureStripe1.TabStop = false;
            // 
            // Info
            // 
            this.Info.Cursor = System.Windows.Forms.Cursors.Default;
            this.Info.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Info.Location = new System.Drawing.Point(11, 98);
            this.Info.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(284, 388);
            this.Info.TabIndex = 21;
            this.Info.Text = resources.GetString("Info.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(223, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(417, 24);
            this.label2.TabIndex = 23;
            this.label2.Text = "Количество деталей по категориям в штуках";
            // 
            // label1
            // 
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(394, 98);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 377);
            this.label1.TabIndex = 24;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // Ростов_на_Дону
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(827, 544);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Ростов_на_Дону";
            this.Text = "Ростов_на_Дону";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureStripe2;
        private System.Windows.Forms.Label Back;
        private System.Windows.Forms.PictureBox pictureStripe1;
        private System.Windows.Forms.Label Info;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}