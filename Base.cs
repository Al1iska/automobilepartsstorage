﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AutomobilePartsStorage
{
    public partial class Base : Form
    {
        public Base()
        {
            InitializeComponent();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void closeButton_MouseEnter_1(object sender, EventArgs e)
        {
            closeButton.ForeColor = Color.Red;
        }

        Point lastPoint;
        private void panel_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void closeButton_MouseLeave_1(object sender, EventArgs e)
        {
            closeButton.ForeColor = Color.Black;
        }

        private void Info_Click(object sender, EventArgs e)
        {
            Information newForm = new Information();
            newForm.Show();
            this.Hide();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Form1 newForm = new Form1();
            newForm.Show();
            this.Hide();
        }

        private void panel_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void Company_Click(object sender, EventArgs e)
        {
            Manual newForm = new Manual();
            newForm.Show();
            this.Hide();
        }

        private void Info_Click_1(object sender, EventArgs e)
        {
            Information newForm = new Information();
            newForm.Show();
            this.Hide();
        }
        private void Add_Click(object sender, EventArgs e)
        {
            if (CountryBox.Text != "" && MarkBox.Text != "" && CategoryBox.Text != "" && PriceBox.Text != "" && IndexBox.Text != "")
            {
                string c = CountryBox.Text;
                textBox1.Text += c + Environment.NewLine;
                string m = MarkBox.Text;
                textBox1.Text += m + Environment.NewLine;
                string ct = CategoryBox.Text;
                textBox1.Text += ct + Environment.NewLine;
                int p;
                p = Convert.ToInt32(PriceBox.Text);
                textBox1.Text += p.ToString() + Environment.NewLine;
                int i;
                i = Convert.ToInt32(IndexBox.Text);
                textBox1.Text += i.ToString() + Environment.NewLine + Environment.NewLine;
            }
            else
                MessageBox.Show("Введите все данные!");
        }
    }
}