﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AutomobilePartsStorage
{
    public partial class Information : Form
    {
        public Information()
        {
            InitializeComponent();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Base newForm = new Base();
            newForm.Show();
            this.Hide();
        }

        Point lastPoint;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void Info_Click(object sender, EventArgs e)
        {
             
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
