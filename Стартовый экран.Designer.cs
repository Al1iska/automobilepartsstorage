﻿
namespace AutomobilePartsStorage
{
    partial class Стартовый_экран
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.Adm = new System.Windows.Forms.Button();
            this.Sotr = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Label();
            this.pictureStripe2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureStripe1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.Adm);
            this.panel1.Controls.Add(this.Sotr);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.closeButton);
            this.panel1.Controls.Add(this.pictureStripe2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureStripe1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 450);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // Adm
            // 
            this.Adm.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Adm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Adm.FlatAppearance.BorderSize = 0;
            this.Adm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.Adm.Location = new System.Drawing.Point(465, 243);
            this.Adm.Margin = new System.Windows.Forms.Padding(2);
            this.Adm.Name = "Adm";
            this.Adm.Size = new System.Drawing.Size(133, 57);
            this.Adm.TabIndex = 102;
            this.Adm.Text = "Администратор";
            this.Adm.UseVisualStyleBackColor = false;
            this.Adm.Click += new System.EventHandler(this.Adm_Click);
            // 
            // Sotr
            // 
            this.Sotr.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Sotr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sotr.FlatAppearance.BorderSize = 0;
            this.Sotr.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumBlue;
            this.Sotr.Location = new System.Drawing.Point(191, 243);
            this.Sotr.Margin = new System.Windows.Forms.Padding(2);
            this.Sotr.Name = "Sotr";
            this.Sotr.Size = new System.Drawing.Size(133, 57);
            this.Sotr.TabIndex = 101;
            this.Sotr.Text = "Сотрудник";
            this.Sotr.UseVisualStyleBackColor = false;
            this.Sotr.Click += new System.EventHandler(this.Sotr_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(252, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(268, 61);
            this.label1.TabIndex = 100;
            this.label1.Text = "Войти как:";
            // 
            // closeButton
            // 
            this.closeButton.AutoSize = true;
            this.closeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.closeButton.Location = new System.Drawing.Point(767, 36);
            this.closeButton.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(22, 25);
            this.closeButton.TabIndex = 24;
            this.closeButton.Text = "x";
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // pictureStripe2
            // 
            this.pictureStripe2.Image = global::AutomobilePartsStorage.Properties.Resources.Blue_stripe;
            this.pictureStripe2.Location = new System.Drawing.Point(0, 416);
            this.pictureStripe2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureStripe2.Name = "pictureStripe2";
            this.pictureStripe2.Size = new System.Drawing.Size(800, 34);
            this.pictureStripe2.TabIndex = 23;
            this.pictureStripe2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AutomobilePartsStorage.Properties.Resources.A1_Motors_logo1;
            this.pictureBox1.Location = new System.Drawing.Point(673, 90);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // pictureStripe1
            // 
            this.pictureStripe1.Image = global::AutomobilePartsStorage.Properties.Resources.Blue_stripe;
            this.pictureStripe1.Location = new System.Drawing.Point(0, 0);
            this.pictureStripe1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureStripe1.Name = "pictureStripe1";
            this.pictureStripe1.Size = new System.Drawing.Size(800, 34);
            this.pictureStripe1.TabIndex = 12;
            this.pictureStripe1.TabStop = false;
            // 
            // Стартовый_экран
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Стартовый_экран";
            this.Text = "Стартовый_экран";
            this.Load += new System.EventHandler(this.Стартовый_экран_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureStripe1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureStripe1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureStripe2;
        private System.Windows.Forms.Label closeButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Adm;
        private System.Windows.Forms.Button Sotr;
    }
}